# -*- coding: utf-8 -*-

import unicodedata

def mreplace(s, chararray, newchararray):
    """
    Zamienia wszystkie wystąpienia znaków w napisie s z tablicy chararray na odpowiednie wystąpienia w tablicy newchararray. 
    """
    for a, b in zip(chararray, newchararray):
        s = s.replace(a, b)
    return s

def utf2ascii(s):
    chararray =    [u'ą',u'ć',u'ę',u'ł',u'ń',u'ó',u'ś',u'ź',u'ż',u'Ą',u'Ć',u'Ę',u'Ł',u'Ń',u'Ó',u'Ś',u'Ź',u'Ż',u'č',u'ö', u'–', u'„', u'”', u'í']
    newchararray = [u'a',u'c',u'e',u'l',u'n',u'o',u's',u'z',u'z',u'A',u'C',u'E',u'L',u'N',u'O',u'S',u'Z',u'Z',u'c',u'o', u'-', u'"', u'"', u'i']
    return mreplace(s, chararray, newchararray)
