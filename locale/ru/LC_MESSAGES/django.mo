��          �      <      �     �     �     �     �     �     �     �            3   '     [     v     ~     �  %   �     �     �  �       �     �               .  '   ?  !   g  !   �  )   �  e   �  8   ;     t  A   �  =   �  q   	  B   {  H   �                                	                         
                                            Applications Classes Timetable Contact Contact Details Contact Name Courses E-mail Graduate Poll Graduate declaration Internal Server Error. Contact with administrator:  Inventory Management Panel Results Syllabus Management Panel Syllabus Search Engine Take part in Course Evaluation Survey Teachers Workload Management Teaching Offer Management Project-Id-Version: syjon
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-30 18:31+0200
PO-Revision-Date: 2013-09-26 18:28+0100
Last-Translator: Piotr Wierzgała <piotr.wierzgala@umcs.lublin.pl>
Language-Team: syjon <syjon@umcs.lublin.pl>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Приложения График занятий Контакт Подробности Название Направления обучения Электронный адрес Анкета выпускника Декларация выпускника Внутренняя ошибка сервера. Свяжитесь с администратром. Панель управления инвентарием Результаты Панель управления научными планами Поисковая система научных планов Принять участие в анкетированию по оценке проведения занятий Управление дидактической нагрузкой Управление дидактическим предложением 