��          �      L      �     �     �     �     �     �                    "  3   7     k  �   �     c     k     �  %   �     �     �  �  �  	   �     �     �     �     �     �     �     �       =   )     g  �   �     �     �     �  %   �     �     �                               	                         
                                            Applications Classes Timetable Contact Contact Details Contact Name Courses E-mail Graduate Poll Graduate declaration Internal Server Error. Contact with administrator:  Inventory Management Panel On this page you can find syllabuses for first and second year of bachelor and master courses. Other syllabuses can be found on: <a class='link_0' href='http://old.sjk.umcs.lublin.pl/'>http://old.sjk.umcs.lublin.pl/</a>. Results Syllabus Management Panel Syllabus Search Engine Take part in Course Evaluation Survey Teachers Workload Management Teaching Offer Management Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-30 18:31+0200
PO-Revision-Date: 2014-01-16 10:48+0100
Last-Translator: Piotr Wierzgała <piotr.wierzgala@umcs.lublin.pl>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Aplikacje Rozkład zajęć Kontakt Dane kontaktowe Imię i Nazwisko Kierunki E-mail Ankieta absolwentów Deklaracja absolwenta Wystąpił błąd serwera. Skontaktuj się z administratorem: Panel zarządzania inwentarzem Na tej stronie znajdują się sylabusy dla pierwszego i drugiego roku studiów licencjackich i magisterskich. Pozostałe sylabusy można znaleźć na stronie: <a class='link_0' href='http://old.sjk.umcs.lublin.pl/'>http://old.sjk.umcs.lublin.pl/</a>. Wyniki Panel zarządzania sylabusami Wyszukiwarka sylabusów Weź udział w Ankiecie Oceny Zajęć Obsada Zajęć Moje kierunki 