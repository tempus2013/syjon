# -*- coding: utf-8 -*-
from django.conf import settings

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.utils import translation

TEMPLATE_ROOT = 'syjon/'


def home(request):

    installed_apps = []
    for app_name in settings.INSTALLED_APPS:
        app_details = app_name.split('.')
        if app_details[0] == 'apps':
            installed_apps.append(app_details[1])

    kwargs = {'installed_apps': installed_apps}

    return render_to_response(TEMPLATE_ROOT+'syjon.html', {}, context_instance=RequestContext(request, kwargs))


def contact(request):

    kwargs = {'contacts': settings.CONTACT_INFO}

    return render_to_response(TEMPLATE_ROOT+'contact.html', {}, context_instance=RequestContext(request, kwargs))

