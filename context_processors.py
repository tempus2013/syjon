# -*- coding: utf-8 -*-

from apps import merovingian, pythia, niobe

def consts(request):
    return {
            'VERSION_SYJON': '1.0.0',
            'VERSION_MEROVINGIAN': merovingian.VERSION,
            'VERSION_PYTHIA': pythia.VERSION,
            'VERSION_NIOBE': niobe.VERSION,
            }
